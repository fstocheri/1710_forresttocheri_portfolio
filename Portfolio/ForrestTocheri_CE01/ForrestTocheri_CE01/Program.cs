﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ForrestTocheri_CE01
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] fishColors = new string[12];
            fishColors[0] = "red";
            fishColors[1] = "blue";
            fishColors[2] = "green";
            fishColors[3] = "purple";
            fishColors[4] = "red";
            fishColors[5] = "green";
            fishColors[6] = "blue";
            fishColors[7] = "purple";
            fishColors[8] = "purple";
            fishColors[9] = "red";
            fishColors[10] = "blue";
            fishColors[11] = "blue";
            float[] fishSizes = new float[12];
            fishSizes[0] = 2.3F;
            fishSizes[1] = 22.11F;
            fishSizes[2] = 12.4F;
            fishSizes[3] = 8.32F;
            fishSizes[4] = 14.75F;
            fishSizes[5] = 13.23F;
            fishSizes[6] = 25.25F;
            fishSizes[7] = 25.85F;
            fishSizes[8] = 20.9F;
            fishSizes[9] = 18.0F;
            fishSizes[10] = 18.5F;
            fishSizes[11] = 4.15F;

            //int count = fishSizes.Count();
            //for (int i = 0; i < count; i++)
            //{
            //    Console.WriteLine(fishSizes[i].ToString());
            //    Console.WriteLine(fishColors[i].ToString());
            //}

            //prompt user to select a color
            Console.WriteLine("Please select a color of fish:");
            Console.WriteLine("1. Red\r\n2. Blue\r\n3. Green\r\n4. Purple");
            //save the user's response
            string userResponse = Console.ReadLine();
            //create an int for conversion
            int userResponseInt;
            //check that the user did not leave the field empty
            while(string.IsNullOrEmpty(userResponse))
            {
                Console.WriteLine("Please do not leave blank.");
                Console.WriteLine("Please select a color of fish:");
                Console.WriteLine("1. Red\r\n2. Blue\r\n3. Green\r\n4. Purple");
                userResponse = Console.ReadLine();
            }
            //check that the user entered a number, and that it is between 1-4
            while (!int.TryParse(userResponse, out userResponseInt) || userResponseInt <= 0 || userResponseInt > 4)
            {
                Console.WriteLine("Please only enter one of the numbers shown.\r\nPlease select a color of fish:");
                Console.WriteLine("1. Red\r\n2. Blue\r\n3. Green\r\n4. Purple");
                userResponse = Console.ReadLine();
            }
            //now that we have confirmed that the user entered a number and that it is within our range
            //we will check for each color
            //checking for the 1st selection "red"
            if (userResponseInt == 1)
            {
                //create an arraylist that will hold all of the size values where the color is red
                //we will be using the same method to check and count each other color
                ArrayList redSizes = new ArrayList();
                //count through the array and find all the fish with the color selected
                for (int i = 0; i < fishColors.Length; i++)
                {
                    //when we find the color we are looking for
                    if (fishColors[i] == "red")
                    {
                        //we will add the size that correlates to our new arraylist
                        redSizes.Add(fishSizes[i]);
                    }
                }
                //sort the arraylist from smallest to largest
                redSizes.Sort();
                //reverse that order so that the largest is first
                redSizes.Reverse();
                //display to the user the largest red fish
                Console.WriteLine($"The largest red fish has a length of {redSizes[0]}");
            }
            //checking for the 2nd selection "blue"
            else if (userResponseInt == 2)
            {
                ArrayList blueSizes = new ArrayList();
                for (int i = 0; i < fishColors.Length; i++)
                {
                    if(fishColors[i] == "blue")
                    {
                        blueSizes.Add(fishSizes[i]);
                    }
                }
                blueSizes.Sort();
                blueSizes.Reverse();
                Console.WriteLine($"The largest blue fish has a length of {blueSizes[0]}");
            }
            //checking for the 3rd selection "green"
            else if(userResponseInt == 3)
            {
                ArrayList greenSizes = new ArrayList();
                for (int i = 0; i < fishColors.Length; i++)
                {
                    if (fishColors[i] == "green")
                    {
                        greenSizes.Add(fishSizes[i]);
                    }
                }
                greenSizes.Sort();
                greenSizes.Reverse();
                Console.WriteLine($"The largest green fish has a length of {greenSizes[0]}");
            }
            //checking for the 4th selection "purple"
            else if(userResponseInt == 4)
            {
                ArrayList purpleSizes = new ArrayList();
                for (int i = 0; i < fishColors.Length; i++)
                {
                    if (fishColors[i] == "purple")
                    {
                        purpleSizes.Add(fishSizes[i]);
                    }
                }
                purpleSizes.Sort();
                purpleSizes.Reverse();
                Console.WriteLine($"The largest purple fish has a length of {purpleSizes[0]}");
            }
        }
    }
}
