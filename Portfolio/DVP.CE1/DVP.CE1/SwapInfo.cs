﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP.CE1
{
    //Forrest Tocheri
    //1710
    //SwapInfo Class Challenge 1
    //Swap the info given by the user
    class SwapInfo
    {
        //create member variables
        string mFirstName;
        string mLastName;

        //create constructor function
        public SwapInfo(string _firstName, string _lastName)
        {
            mFirstName = _firstName;
            mLastName = _lastName;
        }

        //create getter functions for each member variable
        public string getFirstName()
        {
            return mFirstName;
        }
        public string getLastName()
        {
            return mLastName;
        }

        //create setter functions for each member variable
        //will ask that the user set the first and last name when called
        public void setFirstName()
        {
            //prompt user for first name
            Console.WriteLine("Please enter your first name:");
            //save the users entered name to variable
            mFirstName = Console.ReadLine();
            //validate that the user entered something
            while (string.IsNullOrEmpty(mFirstName))
            {
                Console.WriteLine("Please do not leave blank.\r\nPlease enter your first name:");
                mFirstName = Console.ReadLine();
            }
            //dispaly the name entered to user
            Console.WriteLine("Your first name is {0}", mFirstName);
        }
        //create another setter for the last name
        //same layout as first name
        public void setLastName()
        {
            Console.WriteLine("Please enter your last name:");
            mLastName = Console.ReadLine();
            while (string.IsNullOrEmpty(mLastName))
            {
                Console.WriteLine("Please do not leave blank.\r\nPlease enter your last name:");
                mLastName = Console.ReadLine();
            }
            Console.WriteLine("Your last name is {0}", mLastName);
        }

        //create function that will swap the info
        //swapped info is displayed to the user
        public void swapInfo()
        {
            Console.WriteLine("The reverse of your name is {0}, {1}", mLastName, mFirstName);
        }
    }
}
