﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP.CE1
{
    //Forrest Tocheri
    //1711
    //Backwards class challenge 2
    //Print the user enetered sentence backwards
    class Backwards
    {
        //member variables
        int numWords;
        string userSentance;

        //constructor function
        public Backwards(string _userSentance)
        {
            userSentance = _userSentance;
        }

        //setter function to set the sentence to what the user enters
        public void setSentance()
        {
            //prompt for user sentence
            Console.WriteLine("Please enter a sentance conataining at least six(6) words, seperated by spaces:");
            //save the sentence
            userSentance = Console.ReadLine();
        }

        //getter function to print out the sentence
        public string getSentance()
        {
            return userSentance;
        }

        //function to count the words used in the user's sentence
        public int countWords()
        {
            //create variables to use within the while loop
            int index = 0, wordCount = 1;
            //parse through the user entered sentence
            while (index <= userSentance.Length - 1)
            {
                //check for spaces or other seperators
                if (userSentance[index] == ' ' || userSentance[index] == '\n' || userSentance[index] == '\t')
                {
                    //when seperators are found, count as a word
                    wordCount++;
                }
                //continue through sentence
                index++;
            }
            //set the member variable
            numWords = wordCount;
            //display the amount of words in the user's sentence
            Console.WriteLine("There are {0} words in your sentence.", numWords);
            return numWords;
        }

        //function to reverse the string
        public void reverseString()
        {
            //create a character array using user's sentence
            char[] reverseString = userSentance.ToCharArray();
            //call the reverse function on the new array
            Array.Reverse(reverseString);
            //convert the new reversed array to a string
            string newString = new string(reverseString);
            //print out the new reversed string
            Console.WriteLine("Your reversed sentence is: {0}", newString);
        }
        
    }
}
