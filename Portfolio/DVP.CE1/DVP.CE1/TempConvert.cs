﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP.CE1
{
    //Forrest Tocheri
    //1711
    //TempConvert challenge #4
    //convert the user entered temp from 
    class TempConvert
    {
        //create member variables
        int tempF;
        int tempC;

        //create constructor function
        public TempConvert(int _tempF, int _tempC)
        {
            _tempF = tempF;
            _tempC = tempC;
        }

        //create setter functions
        public void setTempF()
        {
            Console.WriteLine("Please enter a temperature in Fahrenheit:");
            tempF = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("You have entered {0} degrees Fahrenheit", tempF);
        }
        public void setTempC()
        {
            Console.WriteLine("Please enter a temperature in Celsius:");
            tempC = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("You have entered {0} degrees Celsius", tempC);
        }

        //create getter functions
        public int getTempF()
        {
            return tempF;
        }
        public int getTempC()
        {
            return tempC;
        }

        //crete functiopn to convert F to C
        public void convertionF()
        {
            decimal convertedToC;
            convertedToC = (tempF - 32) * (5 / 9);
            Console.WriteLine("Your entered Fahrenheit, {0}, is {1} degrees Celsius.", tempF, convertedToC); 
        }
        public void convertionC()
        {
            decimal convertedToF;
            convertedToF = (tempC * (9/5)) + 32;
            Console.WriteLine("Your entered Celsius, {0}, is {1} degrees Fahrenheit.", tempC, convertedToF);
        }
    }
}
