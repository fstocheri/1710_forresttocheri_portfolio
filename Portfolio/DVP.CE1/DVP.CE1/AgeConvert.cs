﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP.CE1
{
    //Forrest Tocheri
    //1711
    //AgeConvert challenge #3
    //Convert the age entered to different measures
    class AgeConvert
    {
        //create member variables
        string mName;
        int mAge;

        //create constructor function
        public AgeConvert(string _name, int _age)
        {
            _name = mName;
            _age = mAge;
        }

        //function used to set the users name
        public void setName()
        {
            Console.WriteLine("Please enter your name:");
            mName = Console.ReadLine();
            Console.WriteLine("You have entered: {0}", mName);
        }

        //function used to set the users age
        public void setAge()
        {
            Console.WriteLine("Please enter your age:");
            mAge = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("You have entered: {0}", mAge);
        }

        //function used to get the user's entered name
        public string getName()
        {
            return mName;
        }

        //function used to get the user's entered age
        public int getAge()
        {
            return mAge;
        }

        //function to convert the user's age from years to days
        public void ageToDays()
        {
            //create new variable for days
            int ageDays;
            //calculation from years to days
            ageDays = mAge * 365;
            //print the age in days with the users name
            Console.WriteLine("Your name is {0}, and you have been alive for {1} days!",mName, ageDays);
        }

        //function to convert the users age to hours
        public void ageToHours()
        {
            //create new variable for hours
            int ageHours;
            //years to hours calculation
            ageHours = mAge * 8784;
            //prin the age in hours with the users name
            Console.WriteLine("Your name is {0}, and you have been alive for {1} hours!",mName, ageHours);
        }

        //function to convert age to minutes
        public void ageToMinutes()
        {
            //creeate new variable for minutes
            int ageMinutes;
            //years to minutes calculation
            ageMinutes = mAge * 525600;
            //print the users age in minutes with their name
            Console.WriteLine("Your name is {0}, and you have been alive for {1} minutes!",mName, ageMinutes);
        }

        //function to convert age to seconds
        public void ageToSeconds()
        {
            //create new variable for seconds
            int ageSeconds;
            //years to seconds calculation
            ageSeconds = mAge * 31540000;
            //print the users names with their age in seconds
            Console.WriteLine("Your name is {0}, and you have been alive for {1} seconds!",mName, ageSeconds);
        }
    }
}
