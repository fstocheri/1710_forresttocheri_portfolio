﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP.CE1
{
    class Program
    {
        //Forrest Tocheri
        //1710
        //C# Challenges
        //Portfolio Challenges

        //set static variables to be used in the new classes
        static string firstName;
        static string lastName;
        static string customSentance;
        static string name;
        static int age;
        static int tempF1;
        static int tempC1;

        static void Main(string[] args)
        {
            int userCount = 0;
            while(userCount < 100)
            {
                //variable that will save the choice our user has made
                int userSelection;
                //explain to the user what to do
                Console.WriteLine("Please type 1 through 4 for your selection:");
                Console.WriteLine("1)SwapInfo\r\n2)Backwards\r\n3)AgeConvert\r\n4)TempConvert");
                //save the users choice to the previous variable
                userSelection = Convert.ToInt32(Console.ReadLine());
                //if/else statements to correspond with the user's selected class to run.
                if(userSelection == 1)
                {
                    //create an instance of the new class
                    SwapInfo reversName = new SwapInfo(firstName, lastName);
                    //Run the set function for first and last name
                    reversName.setFirstName();
                    reversName.setLastName();
                    //present a written line that swaps the first and last name that were given
                    reversName.swapInfo();
                    userCount++;
                }
                else if(userSelection == 2)
                {
                    //create an instance of the new class
                    Backwards reverseSentence = new Backwards(customSentance);
                    //run the function to set the sentence
                    reverseSentence.setSentance();
                    //use while loop to ensure that the entered sentence has at least six words
                    while (reverseSentence.countWords() < 6)
                    {
                        //if the sentence does not have 6 words, ask them to re-do sentence
                        Console.WriteLine("Your sentence must have at least six(6) words.");
                        reverseSentence.setSentance();
                    }
                    //print out the user's sentence
                    Console.WriteLine("Your sentence is\r\n{0}", reverseSentence.getSentance());
                    //print out the reversed sentence
                    reverseSentence.reverseString();
                    userCount++;
                }
                else if(userSelection == 3)
                {
                    //create an instance of the new class
                    AgeConvert ageCalcs = new AgeConvert(name, age);
                    ageCalcs.setName();
                    ageCalcs.setAge();
                    ageCalcs.ageToDays();
                    ageCalcs.ageToHours();
                    ageCalcs.ageToMinutes();
                    ageCalcs.ageToSeconds();
                    userCount++;
                }
                else if(userSelection == 4)
                {
                    //create an instance of the new class
                    TempConvert userConvert = new TempConvert(tempF1, tempC1);
                    userConvert.setTempF();
                    userConvert.convertionF();
                    userConvert.setTempC();
                    userConvert.convertionC();
                    userCount++;
                }
                //final else if the user chooses anything other than 1-4
                else
                {
                    Console.WriteLine("Please only enter 1, 2, 3, or 4.");
                    userCount++;
                }
            }

            ////create an instance of the new class
            //SwapInfo reversName = new SwapInfo(firstName, lastName);
            ////Run the set function for first and last name
            //reversName.setFirstName();
            //reversName.setLastName();
            ////present a written line that swaps the first and last name that were given
            //reversName.swapInfo();

            ////create an instance of the new class
            //Backwards reverseSentence = new Backwards(customSentance);
            ////run the function to set the sentence
            //reverseSentence.setSentance();
            ////use while loop to ensure that the entered sentence has at least six words
            //while(reverseSentence.countWords() < 6)
            //{
            //    //if the sentence does not have 6 words, ask them to re-do sentence
            //    Console.WriteLine("Your sentence must have at least six(6) words.");
            //    reverseSentence.setSentance();
            //}
            ////print out the user's sentence
            //Console.WriteLine("Your sentence is\r\n{0}", reverseSentence.getSentance());
            ////print out the reversed sentence
            //reverseSentence.reverseString();

            ////create an instance of the new class
            //AgeConvert ageCalcs = new AgeConvert(name, age);
            //ageCalcs.setName();
            //ageCalcs.setAge();
            //ageCalcs.ageToDays();
            //ageCalcs.ageToHours();
            //ageCalcs.ageToMinutes();
            //ageCalcs.ageToSeconds();

            ////create an instance of the new class
            //TempConvert userConvert = new TempConvert(tempF1, tempC1);
            //userConvert.setTempF();
            //userConvert.convertionF();
            //userConvert.setTempC();
            //userConvert.convertionC();
        }
    }
}
