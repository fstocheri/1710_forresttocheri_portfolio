//parent class for the 2 different character objects
class character{
	//constructors are just name, hp and attack
	constructor(name, hp, attack){
		//check that the values are filled
		if(name == "" || hp == "" || attack == ""){
			console.log("No field can be blank.");
			return false;
		}
		//fill these values with what was chosen
		else{
			this._name = name;
			this._hp = hp;
			this._attack = attack;
		}
	}
	//different getter and setter functions
	getName(){
		return this._name;
	}
	getHp(){
		return this._hp;
	}
	getAttack(){
		return this._attack;
	}
	setName(nName){
		this._name = nName;
	}
	setHp(nHp){
		this._hp = nHp;
	}
	setAttack(hAttack){
		this._attack = nAttack;
	}
	//function to lose a certain amount of hp
	loseHp(nHp){
		this._hp -= nHp;
	}
	//function to gain a certain amount of hp
	gainHp(nHp){
		this._hp += nHp;
	}
	//function to lose a certain amount of attack
	loseAttack(nAttack){
		this._attack -= nAttack;
	}
	//function to gain a certain amount of attack
	gainAttack(nAttack){
		this._attack += nAttack;
	}
}
//child-class that is the user's character
//will inherit everything from the parent class
//including functions
class userCharacter extends character{
	constructor(name, hp, attack){
		super(name, hp, attack)
	}

}
//child-class for the monster characters
//will also inherit everything from the parent class
//flexibility to change the different attributes it could contain
class npcCharacter extends character{
	constructor(name, hp, attack){
		super(name, hp, attack)
	}
}

//a function to get the user's name
function getName(){
	console.clear();
	//prompt the user for their name
	var user = prompt("Please enter your name.")
	//print out the name for the user to see
	console.log("Welcome " + user + "!");
	//return this value for later
	return user;
}
//a function for the user to select their character class
//this will determine what their other attributes are
function getClass(){
	//a do/while function for user input
	//user will select a certain class
	do{
		var uChoice = parseInt(prompt("What class would you like to be?\n1. Cleric(hp)\n2. Warrior(attack)\n3. Knight(balance)", ""), 10);
	}while(isNaN(uChoice) || uChoice > 3 || uChoice < 1);

	//if the user chose cleric
	//set their attributes to the cleric's attributes
	if((uChoice == 1)){
		var hp = 30;
		var attack = 20;
		console.log("You have chosen a mighty Cleric!");
	}
	//if the user chose warrior
	//set their attributes to the warrior's attributes
	else if((uChoice == 2)){
		var hp = 20;
		var attack = 30;
		console.log("You have chosen a bloodthirsty Warrior!");
	}
	//if the user chose knight
	//set their attributes to the knight's attributes
	else if((uChoice == 3)){
		var hp = 25;
		var attack = 25;
		console.log("You have chosen a dutiful Knight!");
	}
	//should never run
	//used for testing
	else{
		console.log("I should not be here");
	}
	//return the 2 unique values in an array
	//that way we can have a function return 2 values
	return [hp, attack];
}
//function to create a list of monsters with different attributes
//are in order of weakest to strongest
//in function so that we can add and remove easily
function monsterList(){
	let rat = new npcCharacter("Rat", 20, 2);
	let skeleton = new npcCharacter("Skeleton", 30, 3);
	let zombie = new npcCharacter("Zombie", 40, 5);
	let dragon = new npcCharacter("Dragon", 50, 10);
	let demon = new npcCharacter("Demon", 60, 6);
	let kraken = new npcCharacter("Kraken", 70, 12);
	//save as an array for later
	var monsterList = [rat, skeleton, zombie, demon, dragon, kraken];
	//return value to save to variable later
	return monsterList;
}
//function used as an extention of math.random
//this will only return full int's
//use to avoid decimals
//whole numbers are prettier
function getRandomInt(max){
	return Math.floor(Math.random() * Math.floor(max));
}
//the function for the battle phase
//this will take a monster argument
//will be used for every monster encounter
function battlePhase(monster){
	//check that the player is still alive
	if(player1.getHp() > 0){
		//let the player know what they have encountered
		console.log("A wild " + monster.getName() + " has appeared!");
		//begin combat
		//large "-" used for clearer console reading
		console.log("-----------------------Begin combat!-----------------------");
		//while loop to check that the player and the monster are alive
		while((monster.getHp() > 0) && (player1.getHp() > 0)){
			//player gets an opportunity to attack with our random number generator
			var playerAttackValue = getRandomInt(player1.getAttack());
			//the monster will lose this much hp
			monster.loseHp(playerAttackValue);
			//check if the value is 0
			if(playerAttackValue == 0){
				//if so, the player missed
				console.log("You missed!");
			}
			//else you show how much damage was done
			else{
				console.log("You deal " + playerAttackValue + " damage!");
			}
			//if the monster still has hp left
			if(monster.getHp() > 0){
				//show how much hp they have left
				console.log("The " + monster.getName() + " lives with " + monster.getHp() + " hp.");
				//monster's turn to attack
				console.log("Oh no! It's attacking!");
				//random number generator used to get a monster attack value
				var monsterAttackValue = getRandomInt(monster.getAttack());
				//player loses that much hp
				player1.loseHp(monsterAttackValue);
				//check if the monster missed
				if(monsterAttackValue == 0){
					console.log("It missed!");
				}
				//else you show how much damage was done
				else{
					console.log("You take " + monsterAttackValue + " damage! Ouch!");
					//check that the player is out of hp
					//if so, show game over
					if(player1.getHp() <= 0){
						console.log("You have fainted. Your Dungeon Run ends here...");
						console.log("GAME OVER");
					}
					//if they are still alive
					//show how much hp is left
					else{
						console.log("You have " + player1.getHp() + " hp left.");
					}
				}
			}
			//if the monster does not have hp
			//show that you have defeated the monster
			else{
				console.log("You have defeated a " + monster.getName());
			}
		}
	}
	else{
		return;
	}
}
//function to run after each battle phase
//will allow the player to choose an upgrade as a reward
function lootPhase(){
	do{
		var input = parseInt(prompt("Select your reward:\n1. 10 Hp\n2. 5 Attack", ""), 10);
	}while(isNaN(input) || input < 1 || input > 2);
	//gain 10 hp if they chose hp
	if((input == 1)){
		player1.gainHp(10);
	}
	//gain 5 attack if they chose attack
	else{
		player1.gainAttack(5);
	}
}

//game running by calling above functions
//get the name of the user
var userName = getName();
//get their attribute selection
var userStats = getClass();
//create a player with those values
let player1 = new userCharacter(userName, userStats[0], userStats[1]);
//call the monster list
var monsterList = monsterList();
//itterate through the list of monsters 1 at a time
//the user will have a battle phase for each monster until they are dead or win
for(var i = 0; i < monsterList.length; i++){
	battlePhase(monsterList[i]);
	//if the user wins and is alive
	if(i == monsterList.length - 1 && player1.getHp() > 0){
		console.log("You have done it " + player1.getName() + ". You are a champion!");
	}
	//after each phase, the user gets to loot if they are still alive
	else if(player1.getHp() > 0){
		lootPhase();
	}
}


//TESTS//
// console.log(player1.getName());
// console.log(player1.getHp());
// console.log(player1.getAttack());
