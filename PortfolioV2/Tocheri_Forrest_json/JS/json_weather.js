//create a JSON object that contains the data from weatherData.txt

var data = {
	"created":"2014-10-17T14:42:39Z", 
	"lang":"en-us", 
	"location":[{"city":"Orlando", "county":"United States", "region":"FL"}], 
	"results":[{"units":[{"distance":"mi", "pressure":"ins","speed":"mph","temperature":"F"}]}],
	"wind":[{"chill":76, "direction":360, "speed":5}],
	"atmosphere":[{"humidity":48,"pressure":29.65,"rising":0,"visibility":10}],
	"astronomy":[{"sunrise":"7:26 am", "sunset":"6:51 pm"}],
	"condition":[{"code":33, "date":"Fri, 17 Oct 2014 5:53 am EST", "temp": 76, "text":"Fair"}],
	"forecast":[
	{"code":30, "date": "17 Oct 2014", "day": "Fri", "high": 80, "low": 62, "text": "Sunny"},
	{"code":30, "date": "18 Oct 2014", "day": "Sat", "high": 82, "low": 63, "text": "Partly Cloudy"},
	{"code":30, "date": "19 Oct 2014", "day": "Sun", "high": 85, "low": 65, "text": "Partly Cloudy"}],
}
//used for testing
//console.log(data.forecast[2].day);

//a for loop to iterate through the length of our created forecast JSON array
//will print out, in sentence form, the day, high, low, and condition(text)
for(var i = 0; i < data.forecast.length; i++){
	console.log("Today is " + data.forecast[i].day + ". The high is " + data.forecast[i].high + ". The low is " + data.forecast[i].low + ". It will be " + data.forecast[i].text + ".");	
}
//user JSOON.parse() to change the JSON text object to a JSON object
//will allow us to grab data from the object
var obj = JSON.parse(text);
//will loop through the amount of employees in array
//will also grab info from the myJSON object in data.js
//will combine the two to show first name, last name, and their occupation
for(var i = 0; i < obj.employees.length; i++){
	console.log(obj.employees[i].firstName + " " + obj.employees[i].lastName + " works as a " + myJSON.people[i].job + ".");
}