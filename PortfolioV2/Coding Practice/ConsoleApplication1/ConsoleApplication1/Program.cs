﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace DatabaseReview
{
    class Program
    {

        static void Main(string[] args)
        {

            MySqlConnection _con = null;

            string cs = @"server=192.168.1.7;userid=dbsAdmin;password=password;database=SampleAPIData;port=8889";

            Console.Write("Please enter a City:");
            string searchTerm = Console.ReadLine();

            try
            {
                _con = new MySqlConnection(cs);
                MySqlDataReader rdr = null;
                _con.Open();

                string check = $"SELECT * from weather where city like '{searchTerm}'";
                MySqlCommand sqlCheck = new MySqlCommand(check, _con);

                MySqlDataReader reader = sqlCheck.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Close();
                    string stm = $"SELECT * from weather where city like '{searchTerm}'";
                    MySqlCommand cmd = new MySqlCommand(stm, _con);

                    rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Console.WriteLine($"Temp: {rdr.GetDecimal(4)} Pressure: {rdr.GetDecimal(5)} Humidity: {rdr.GetInt32(6)}");
                    }
                }
                else
                {
                    Console.WriteLine("No data available for the selected city.");
                }
                reader.Close();


            }
            catch (MySqlException ex)
            {
                Console.WriteLine($"Error: {ex.ToString()}");
            }
            finally
            {
                if (_con != null)
                {
                    _con.Close();
                }
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
                Console.WriteLine("Done");
            }


        }
    }
}
