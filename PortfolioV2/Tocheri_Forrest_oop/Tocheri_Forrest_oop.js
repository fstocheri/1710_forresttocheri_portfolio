//Parent class of Animal
//this will be the base class for different type of animals
class Animal{
	//constructor function for declaring a new animal
	constructor(name, size, location){
		//check that the entered values are not empty
		if(name == "" || size == "" || location == ""){
			console.log("Name and size must not be blank");
			return false;
		}
		//set the entered values to this new animal
		else{
			this._name = name;
			this._size = size;
			this._location = location;
		}
	}
	//getters and setter for the values of an animal
	//can be used by any sub-class
	//"_"'s are used to encapsulate the variables
	//although there are no true private variables in javascript
	//the "_" is used to show the user that is private
	setName(nName){
		this._name = nName;
	}
	setSize(nSize){
		this._size = nSize;
	}
	setLocation(nLocation){
		this._location = nLocation;
	}
	getName(){
		return this._name;
	}
	getSize(){
		return this._size;
	}
	getLocation(){
		return this._location;
	}
	//function to return the name and location of the animal in question
	getAnimal(){
		return "I am a " + this._name + " and I live in " + this._location + "."
	}
}
//sub-class that will inherit from the animal 
//will inherit the get functions as well as the declaration of the animal
//will add a new function to add to a list of just mammals
class Mammal extends Animal{
	constructor(name, size, location){
		super(name, size, location);
	}
}
//sub-class that will inherit from animal
//has the sam constructors as animal
//super is used to inherit those constructors
//Polymorphism is used to add a new constructor
class Reptile extends Animal{
	constructor(name, size, location, poison){
		super(name, size, location);
		this._poison = poison;
	}
	getPoison(){
		return this._poison;
	}
}
//instantiate a new object from the parent class
let gwShark = new Animal("Great White Shark", "very large", "The Atlantic Ocean");
//print out the function for the new animal
console.log(gwShark.getAnimal());
//instantiate a new object in the child-class
let gecko = new Reptile("Gecko", "small", "South America", "no");
//print out a function that was declared in the parent class
console.log(gecko.getSize());
//call a function from the parent class to change the new animal size
gecko.setSize("large");
//print out the new size
console.log(gecko.getSize());
//instantiate a new object of a different child-class
let panda = new Mammal("Panda", "small", "China");
//print out the name using a parent function
console.log(panda.getName());