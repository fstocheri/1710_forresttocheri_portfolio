﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Tocheri_Forrest_ArrayLists
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Forrest Tocheri
             September 17, 2017
             Array Lists
             */

            //main method is just being used to call our new arraylist function
            sportsArrayLists();
        }

        //create a new arraylist function
        public static void sportsArrayLists()
        {
            //first array has a list of sports teams
            ArrayList sportsTeams = new ArrayList();
            sportsTeams.Add("Vikings");
            sportsTeams.Add("Bears");
            sportsTeams.Add("Packers");
            sportsTeams.Add("Patriots");

            //the second array has a list of locations for those sports teams
            ArrayList teamLocation = new ArrayList();
            teamLocation.Add("Minnesota");
            teamLocation.Add("Chicago");
            teamLocation.Add("Green Bay");
            teamLocation.Add("New England");

            //count the length of the arraylist
            int count = sportsTeams.Count;
            //create a for loop to go through every entry of the arraylist
            for(int i = 0; i < count; i++)
            {
                //combine the arraylist values from each list to one sentence
                Console.WriteLine("The " + sportsTeams[i].ToString() + " are from " + teamLocation[i].ToString());
            }

            //remove 2 entries from each of the arraylists
            sportsTeams.Remove("Patriots");
            sportsTeams.Remove("Packers");
            teamLocation.Remove("New England");
            teamLocation.Remove("Green Bay");

            //add one new item to each arraylist at the start of the list
            sportsTeams.Insert(0, "Titans");
            teamLocation.Insert(0, "Tennessee");

            //add space for better looking code
            Console.WriteLine("\r\n");

            //create another for loop counting through the arraylists again, just as before.
            int newCount = sportsTeams.Count;
            for(int i = 0; i < newCount; i++)
            {
                Console.WriteLine("The " + sportsTeams[i].ToString() + " are from " + teamLocation[i].ToString());
            }
        }
    }
}
