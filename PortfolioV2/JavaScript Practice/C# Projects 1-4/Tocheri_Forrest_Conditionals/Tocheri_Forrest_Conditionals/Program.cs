﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tocheri_Forrest_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Forrest Tocheri
             September 8, 2017
             Conditionals
             */

            //Problem #1: Temp Converter

            //Give the user information about what is happening
            Console.WriteLine("Let's convert temperatures!\r\nI will ask for a temperature number and then the degree it is in.");
            //Ask the user for the temperature number and store that in a variable
            Console.WriteLine("Please enter the temperature number:");
            string tempNumberString = Console.ReadLine();
            //convert the temp number from a string to a number
            //use double because the conversion uses decimal numbers
            double tempNumber = double.Parse(tempNumberString);
            //Test that the string was succesfully converted
            //Console.WriteLine(tempNumber+2);
            //Ask the usert for a temperature degree and save it inside a variable
            Console.WriteLine("Please enter the degree, either C or F:");
            string tempDegree = Console.ReadLine();
            //the degree does not need to be converted
            Console.WriteLine("The temperature you entered is: " + tempNumber + tempDegree);

            //Create a conditional to determine how the degree is converted

            //if the degree that was entered is celcius
            if (tempDegree.ToUpper() == "C")
            {
                //use the conversion formula from celcius to fahrenheit
                double tempNumberF = tempNumber * 1.8 + 32;
                //print the converted number for the user
                Console.WriteLine("The temperature is " + tempNumberF + " degrees fahrenheit");
            }
            //else statement for the other case
            else if (tempDegree.ToUpper() == "F")
            {
                //use the conversion formula from fahrenheit to celcius
                double tempNumberC = (tempNumber - 32) / 1.8;
                //print the converted number for the user
                Console.WriteLine("The temperature is " + tempNumberC + " degrees celcius");
            }
            /*
             TEST:
             32F returns 0C
             100C returns 212F
             50c returns 122F
             32.2C returns 89.96F
             */




            //Problem #2: Last Chance for Gas

            //Explain the purpose to the user
            Console.WriteLine("You are about to pass the last gas station for 200 miles, lets make sure we dont need to stop!");
            //Ask the user for the amount their tank can hold and save that inside a variable
            //Convert that variable to a number. Using double just in case of decimals
            Console.WriteLine("How many gallons does your tank hold?");
            string tankGallonString = Console.ReadLine();
            double tankGallons = double.Parse(tankGallonString);
            //Ask the user for the current tank percentage and save that in a variable
            //Convert the variable to a number
            Console.WriteLine("What is your current tank percentage?");
            string tankPercentString = Console.ReadLine();
            double tankPercent = double.Parse(tankPercentString);
            //Convert the whole number entered to a decimal for percentage calculations
            tankPercent = tankPercent / 100;
            //Ask the user for their MPG and save that as a variable
            //Convert the MPG to a number
            Console.WriteLine("How many miles per gallon does your vehicle get?");
            string MpgString = Console.ReadLine();
            double Mpg = double.Parse(MpgString);
            //Calculations to set up a conditional
            double driveMiles = (tankGallons * tankPercent) * Mpg;

            //Create a conditional that determines whether they should stop for gas
            //This will compare the driveMiles (or the total miles that the person can drive) with the 200 miles they need to drive

            if (driveMiles >= 200)
            {
                Console.WriteLine("Yes, you can drive " + driveMiles + " more miles and you can make it without stopping for gas!");
            }
            else
            {
                Console.WriteLine("You only have " + driveMiles + " more miles you can drive, better get gas now while you can!");
            }

            /*
             TEST:
             gallons-20, tank-50%, MPG-25
                returns: yes, with 250 miles
             gallons-12, tank-60%, MPG-20
                returns: no, with 144 miles
             gallons-18, tank-80%, MPG-22
                returns:  yes, with 316.8 miles
             */





            //Problem 3: Grade Letter Calculator

            //Explain to the user what this is for
            Console.WriteLine("Let's find the letter grade based off of your grade percentage!");
            //Ask the user for the grade percentage
            Console.WriteLine("What was your grade percentage?");
            //capture the grade entered, and save that to a variable
            string gradeString = Console.ReadLine();
            //convert the string to a number and then that number
            double gradePercent = double.Parse(gradeString);

            //create a conditional to check the percentage with the grade stipulations
            if (gradePercent > 89)
            {
                Console.WriteLine("You have a " + gradePercent + "%" + ", which means you have a(n) A in the class!");
            }
            else if (gradePercent < 90 && gradePercent > 79)
            {
                Console.WriteLine("You have a " + gradePercent + "%" + ", which means you have a(n) B in the class!");
            }
            else if (gradePercent < 80 && gradePercent > 72)
            {
                Console.WriteLine("You have a " + gradePercent + "%" + ", which means you have a(n) C in the class!");
            }
            else if (gradePercent < 73 && gradePercent > 69)
            {
                Console.WriteLine("You have a " + gradePercent + "%" + ", which means you have a(n) D in the class!");
            }
            else if (gradePercent < 70)
            {
                Console.WriteLine("You have a " + gradePercent + "%" + ", which means you have a(n) F in the class!");
            }
            /*
             TEST:
             92 returns 92% and an A
             80 returns 80% and a B
             67 returns 67% and a F
             77 returns 77% and a C
             */






            //Problem 4: Discount Double Check


            //Explain to the user what this does
            Console.WriteLine("We are going to add up the total amount of your items, and determin if you get a discount!");

            //Ask the user for the value of the first item and store that inside of a variable
            Console.WriteLine("Please enter the price of your first item:");
            string firstItemString = Console.ReadLine();
            //Convert the string into a number
            double firstItem = double.Parse(firstItemString);
            //Do the same thing for the second item
            Console.WriteLine("Please enter the price of your second item");
            string secondItemString = Console.ReadLine();
            double secondItem = double.Parse(secondItemString);
            //perform the calculation to get the total amount spent
            double totalCost = firstItem + secondItem;

            //create variables to use in the conditionals for the percentage discounts
            double topDiscount = .90;
            double midDiscount = .95;

            //create a conditional that will compare the total to the discount minimum, and apply or not apply the discount
            if(totalCost >= 100)
            {
                double discountCost = totalCost * topDiscount;
                Console.WriteLine("Your total purchase is $" + discountCost + ", which includes a 10% discount");
            }
            else if(totalCost >=50 && totalCost < 100)
            {
                double discountCost = totalCost * midDiscount;
                Console.WriteLine("Your total purchase is $" + discountCost + ", which includes a 5% discount");
            }
            else
            {
                Console.WriteLine("Your total purchase is $" + totalCost + ".");
            }

            /*
             TEST:
             first item-45.50, second item-75.00
                returns $108.45 with a 10% discount
             first item-30.00, second item 25.00
                returns $52.25 with a 5% discount
             first item-5.75, second item-12.50
                returns $18.25, no discount
             first item-65.98, second item-90.02
                returns $140.40, with a 10% discount
             */

        }
    }
}
