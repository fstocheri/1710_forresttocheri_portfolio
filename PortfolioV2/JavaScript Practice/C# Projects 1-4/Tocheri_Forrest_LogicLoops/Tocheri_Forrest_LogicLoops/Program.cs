﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tocheri_Forrest_LogicLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Forrest Tocheri
             September 8, 2017
             Logic and Loops
             */



            //Problem 1: Logical Operators: Tire Pressure 

            Console.WriteLine("Let's check the tire pressure of your car, and see if your tires are good to go!");

            //Ask the user for the tire pressure in the front right tire
            //convert the captured string to a number for calculations
            Console.WriteLine("What is the pressure of your front right tire?");
            string frontRightString = Console.ReadLine();
            double frontRight = double.Parse(frontRightString);

            //Ask the user for the tire pressure in the back right tire
            //convert the captured string to a number for calculations
            Console.WriteLine("What is the pressure of your back right tire?");
            string backRightString = Console.ReadLine();
            double backRight = double.Parse(backRightString);

            //Ask the user for the tire pressure in the front left tire
            //convert the captured string to a number for calculations
            Console.WriteLine("What is the pressure of your front left tire?");
            string frontLeftString = Console.ReadLine();
            double frontLeft = double.Parse(frontLeftString);

            //Ask the user for the tire pressure in the back left tire
            //convert the captured string to a number for calculations
            Console.WriteLine("What is the pressure of your back left tire?");
            string backLeftString = Console.ReadLine();
            double backLeft = double.Parse(backLeftString);

            //store the variables in arrays
            double[] tirePressure = new double[4] { frontRight, backRight, frontLeft, backLeft };


            if (tirePressure[0] == tirePressure[2] && tirePressure[1] == tirePressure[3])
            {
                Console.WriteLine("The tires pass spec!");
            }
            else
            {
                Console.WriteLine("Get your tires checked out!");
            }


            /*
             TEST:
             Front Left 32, Front Right 32, Back Left 30, Back Right 30
             Results: OK
             Front Left 36, Front Right 32, Back Left 25, Back Right 25
             Results: Not OK, Get checked
             Front Left 20, Front Right 20, Back Left 25, Back Right 26
             Results: Not OK, Get checked
             */






            //Problem 2: Logical Operators: Movie Ticket Price


            //Explain what we are doing to the user
            Console.WriteLine("Welcome to the theater, I need to ask a few questions to determin your ticket price.");

            //Ask the user their age
            Console.WriteLine("How old are you?");
            //save that variable and convert to a number for calculations
            string customerAgeString = Console.ReadLine();
            double customerAge = double.Parse(customerAgeString);

            //Ask the user for the time of day
            Console.WriteLine("What time is the movie you are attending?(in military time)");
            //save that variable and convert to a number for calculations
            string movieTimeString = Console.ReadLine();
            double movieTime = double.Parse(movieTimeString);

            //create variables for the ticket prices
            double yesDiscount = 7.00;
            double noDiscount = 12.00;

            //create conditional to compare the user inputs and determin what price to use
            if (customerAge >= 55 || customerAge <= 10 || (movieTime >= 14 && movieTime <= 17))
            {
                Console.WriteLine("The ticket price is " + "$" + yesDiscount);
            }
            else
            {
                Console.WriteLine("The ticket price is " + "$" + noDiscount);
            }


            /*
             TEST:
             age 57, time 20, ticke price = 7
             age 9, time 20, ticke price = 7
             age 38, time 20, ticket price = 12
             age 25, time 16, ticket price = 7
             age 24, time 24, ticket price = 12
             */




            //Problem 3: For Loop: Add Up the Odds or Evens


            //Explain to the user the purpose of the code
            Console.WriteLine("We are going to find the sum of either the even numbers, or the odd numbers.");
            //Ask the user to choose to find the sum of either even or odd
            //Save that decision in a variable to use later
            Console.WriteLine("Would you like to find the sum of the odd numbers, or even numbers? (odd/even)");
            string userAnswer = Console.ReadLine();

            //create the array of numbers
            int[] numberList = new int[6] { 1, 6, 9, 12, 15, 21 };
            //create a variable for the even numbers
            int evenNumbers = 0;
            //Create a variable for the odd numbers
            int oddNumbers = 0;
            //create a for loop that cycles through the numbers inside the array
            for (int i = 0; i < numberList.Length; i++)
            {
                //create a conditional that checks to see if the array value is divisible by 2 giving remainder of 0 (modulus)
                //if the number leaves any remainder that isnt 0, the number is odd
                if (numberList[i] % 2 == 0)
                {
                    evenNumbers = evenNumbers + numberList[i];
                }
                else
                {
                    oddNumbers = oddNumbers + numberList[i];
                }
            }
            //create a conditional that reads the input of the user determining odd or even request
            if (userAnswer.ToLower() == "even")
            {
                Console.WriteLine("The Even numbers add up to {0}", evenNumbers);
            }
            else
            {
                Console.WriteLine("The Odd numbers add up to {0}", oddNumbers);
            }
            /*
             TEST:
             array: 1,2,3,4,5,6,7, Evens = 12 Odds = 16
             array: 12,13,14,15,16,17, Evens = 42 Odds = 45
             array: 1,6,9,12,15,21, Evens = 18 Odds = 46
             */





            //Problem 4: While Loop: Charge It!
            //Explain the purpose
            Console.WriteLine("We are going to spend a bunch of money, until we hit our credit limit");
            //Ask the user for their limit
            Console.WriteLine("What is your credit limit?");
            //Capture that amount and save it in a variable
            string creditLimitString = Console.ReadLine();
            //convert to a number
            double creditLimit = double.Parse(creditLimitString);

            //create while loop for every time the credit limit remains positive
            while (creditLimit >= 0)
            {
                //Ask user for the purchase amount and save inside a converted variable
                Console.WriteLine("How much was your purchase?");
                string purchaseString = Console.ReadLine();
                double purchaseDouble = double.Parse(purchaseString);
                //subtrace the purchase from the limit while the limit remains above 0
                creditLimit = creditLimit - purchaseDouble;
                //create a conditional to determine what text output is listed, depending on the credit limit being met
                if(creditLimit > 0)
                {
                    Console.WriteLine("With your current purchase of ${0}, you can still spend ${1}", purchaseDouble, creditLimit);

                }
                else
                {
                    //convert the remaining total, or amount over the limit, to a positive and give that back to the user
                    double positiveLimit = Math.Abs(creditLimit);
                    Console.WriteLine("With your last purchase you have reached your credit limit and exceeded it by ${0}", positiveLimit);
                }
            }


            /*
             TEST:
             Credit Limit 20.00
                P1-5.00 - you can spend 15
                P2-12.00 - you can spend 3
                P3-7.00 - you have exeeded limit by 4
             Credit Limit 50.00
                P1-34.90 - you can spend 15.10
                P2-12.88 - you can spend 2.22
                P3-5.90 - you have exeeded limit by 3.68
             */
        }
    }
}
