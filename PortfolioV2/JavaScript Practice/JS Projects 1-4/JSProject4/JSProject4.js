//function to demonstrate an arraylist in Javascript
//created the same as an array
function SportsArrayList(){
	//create array with the team names
	var sportsTeams = ['Vikings', 'Bears', 'Packers', 'Patriots'];
	//create array with the team locations
	var teamLocation = ['Minnesota', 'Chicago', 'Green Bay', 'New England'];
	//create a string for the output to be saved to
	var resultString = "";
	//iterate through the array by value
	for(var i = 0; i<sportsTeams.length; i++){
		//save the string showing team name and where they are from
		resultString += "The " + sportsTeams[i] + " are from " + teamLocation[i] + ".\n";
	}
	//alert out the full string with all of the teams and locations
	alert(resultString);
}
//function to remove and add teams ot the array
function RemoveAddTeams(){
	//create arrat with the team names
	var sportsTeams = ['Vikings', 'Bears', 'Packers', 'Patriots'];
	//create array with the team locations
	var teamLocation = ['Minnesota', 'Chicago', 'Green Bay', 'New England'];
	//create a string for the output to be saved to
	var resultString = "";
	//remove or "pop" 2 values from each array
	sportsTeams.pop("Patriots");
	sportsTeams.pop("Packers");
	teamLocation.pop('New England');
	teamLocation.pop('Green Bay');
	//add or "push" a team into the same arrays
	sportsTeams.push("Titans");
	teamLocation.push("Tennessee");
	//iterate through the array per value
	for(var i = 0; i<sportsTeams.length; i++){
		//save the string showing team name and where they are from
		resultString += "The " + sportsTeams[i] + " are from " + teamLocation[i] + ".\n";
	}
	//alert out the full string with all if the teams and locations
	alert(resultString);
}