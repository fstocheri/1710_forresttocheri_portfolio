//function used to count the number of times a color is present in an array
//the colors are hard coded in the array
function CountFish(){
	//array to hold all the fish colors
	//has repeats
	//four total colors
	var fishColors = ["red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green", "red","blue","green","red","yellow"];
	//save the user's color selection
	var userSelection = document.getElementById('userSelection').value;
	//create a field for the results to display
	var results = document.getElementById('results');
	//check that the user entered a valid input
	//check for NaN
	//check that its between 1 and 4
	if(isNaN(userSelection) || userSelection < 1 || userSelection > 4){
		//alert the user if the input is invalid
		alert("Input not valid")
	}
	//if the input is valid
	else{
		//create variables for the instances of colors to be stored
		var redFish = 0;
		var blueFish = 0;
		var greenFish = 0;
		var yellowFish = 0;
		//iterate through the array and save the colors to the variables when
		//the iteration meets the color
		//will go until there are no values left in the array
		for(var i = 0; i<fishColors.length; i++){
			if(fishColors[i].toLowerCase() == "red"){
				redFish++;
			}
			else if(fishColors[i].toLowerCase() == "blue"){
				blueFish++;
			}
			else if(fishColors[i].toLowerCase() == "green"){
				greenFish++;
			}
			else if(fishColors[i].toLowerCase() == "yellow"){
				yellowFish++;
			}
		}
		//output the totals based on the user's selection
		//will only work if the user selects a valid number
		if(userSelection == 1){
			results.textContent = "In the fish tank there are " + redFish + " fish of the color " + fishColors[0] + ".";
		}
		else if(userSelection == 2){
			results.textContent = "In the fish tank there are " + blueFish + " fish of the color " + fishColors[1] + ".";
		}
		else if(userSelection == 3){
			results.textContent = "In the fish tank there are " + greenFish + " fish of the color " + fishColors[2] + ".";
		}
		else if(userSelection == 4){
			results.textContent = "In the fish tank there are " + yellowFish + " fish of the color " + fishColors[3] + ".";
		}
	}

}