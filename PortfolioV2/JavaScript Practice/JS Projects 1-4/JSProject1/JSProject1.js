//function used to convert C to F
function ToFahrenheit(){
	//save the value of user-entered temp
	var inputField = document.getElementById('tempField').value;
	//set a field to display the result
	var result = document.getElementById('result1');
	//convert the entered number from c to f
	var outputField = (inputField * 1.8) + 32;
	//display the converted temp
	result.textContent = 'Your new temp is: ' + outputField + 'F';
}
//function used to convert F to C
function ToCelcius(){
	//save the value of user-entered temp
	var inputField = document.getElementById('tempField').value;
	//set field to disaply result
	var result = document.getElementById('result1');
	//convert the entered number from f to c
	var outputField = (inputField - 32) / 1.8;
	//display the converted temp
	result.textContent = 'Your new temp is: ' + outputField + 'C';
}
//function used to calculate remaining mileage with user's values
function mpgCalc(){
	//save values for first textbox
	var tankTotal = document.getElementById('tTotal').value;
	//save values for the second textbox
	var tankPercentage = document.getElementById('tPercent').value;
	//convert the percentage to a decimal point
	tankPercentage = tankPercentage / 100;
	//save value for the third textbox
	var mpg = document.getElementById('mpg').value;
	//set field to display results
	var result = document.getElementById('result2');
	//calculate the remaining mileage
	var outputField = (tankTotal * tankPercentage) * mpg;
	//display remaining mileage
	result.textContent = 'You can drive ' + outputField + ' more miles before stopping for gas.'
}
//function used to calculate letter grade based on entered percentage
function gradeLetterCalculator(){
	//save value for first textbox
	var gradePercentage = document.getElementById('gPercent').value;
	//set field to display results
	var result = document.getElementById('result3');
	//if statements to compare the entered number to set conditions
	//will display the users percentage as well as letter grade
	//for A's
	if(gradePercentage > 89){
		result.textContent = 'You have a ' + gradePercentage + '%, which means you have a(n) A in the class.';
	}
	//for B's
	else if(gradePercentage < 90 && gradePercentage > 79){
		result.textContent = 'You have a ' + gradePercentage + '%, which means you have a(n) B in the class.';
	}
	//for C's
	else if(gradePercentage < 80 && gradePercentage > 72){
		result.textContent = 'You have a ' + gradePercentage + '%, which means you have a(n) C in the class.';
	}
	else if(gradePercentage < 73 && gradePercentage > 69){
		result.textContent = 'You have a ' + gradePercentage + '%, which means you have a(n) D in the class.';
	}
	//for D's
	else if(gradePercentage < 70){
		 result.textContent = 'You have a ' + gradePercentage + '%, which means you have a(n) F in the class.';
	}
	//finally for F's
	else{
		result.textContent = 'You entered a value outside of the range.'
	}
}
//function to add item totals and check if the total meets requirements for discounts
function discountCheck(){
	//save value for first textbox
	var item1Cost = document.getElementById('item1').value;
	//dave value for second textbox
	var item2Cost = document.getElementById('item2').value;
	//set field to display results
	var result = document.getElementById('result4');
	//calculate the total by adding item costs
	//+ before variables to do math addition
	var total = +item1Cost + +item2Cost;
	//check to see if total price falls into 2 ranges
	//first range is above a 100 to receive 10% off
	//will display the old total and new total
	if(total >= 100){
		var discountedTotal = total * .90;
		result.textContent = 'Your total is discounted by 10%, down to: $' + discountedTotal + ' from $' + total +'.';
	}
	//second range is between 50-99 to receive 5% off
	//will display old total and new total
	else if(total >= 50 && total < 100){
		var discountedTotal = total * .95;
		result.textContent = 'Your total is discounted by 5%, down to: $' + discountedTotal + ' from $' + total +'.';
	}
	//finally, if the total does not fall within above
	//just display the total, with no discount
	else{
		result.textContent = 'You did not spend enough to receive a discount. Your total is $' + total +'.';
	}
}