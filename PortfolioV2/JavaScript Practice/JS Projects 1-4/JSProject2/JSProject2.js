//function to check the tire pressure by compairing the front 2 and back 2 tires
//the front must be the same and the back must be the same
//front and back do not need to match
function TirePressure(){
	//save value for the first box
	var frPressure = document.getElementById('frontRight').value;
	//save vakue for the second box
	var flPressure = document.getElementById('frontLeft').value;
	//save value for the third box
	var brPressure = document.getElementById('backRight').value;
	//save value for the fourth box
	var blPressure = document.getElementById('backLeft').value;
	//create an array to save all the entered values
	var tirePressure = [frPressure, flPressure, brPressure, blPressure];
	//create a field to display the results
	var result = document.getElementById('result1');
	//check if the pressure in the front 2 tires match and the back 2 tires match
	if(tirePressure[0] == tirePressure[1] && tirePressure[2] == tirePressure[3]){
		//if the tires match the respected tires
		//display that the tires pass the test
		result.textContent = 'The tires pass the test!'
	}
	//if the tires do not match
	else{
		//display that the tires need to be looked at
		result.textContent = 'Get your tires checked out!'
	}
}
//function to get ticket prices based on user data
//age and time are used to see if the movie is discounted
function MovieTicket(){
	//save value for the first box
	var custAge = document.getElementById('age').value;
	//save value for the second box
	var movieTime = document.getElementById('time').value;
	//create a field to display the results
	var result = document.getElementById('result2');
	//variables for discounted and non-discounted prices
	var yesDicount = 7.00;
	var noDiscount = 12.00;
	//check if the user meets requirements for discounted prices
	//user's age must be 55+ or less than 10
	//or the movie time must be 14-17
	if(custAge >= 55 || custAge <= 10 || (movieTime >= 14 && movieTime <=17)){
		//display the discounted price if the values meet requirements
		result.textContent = 'Ticket price is discounted to $' + yesDicount;
	}
	else{
		//display normal cost if the values do not meet requirements
		result.textContent = 'Ticket price is not discounted to $' + noDiscount;
	}
}
//function to add the odd or even numbers of an array
//array is hard-coded
function OddsAndEvens(){
	//save value of first textbox
	var userSelection = document.getElementById('userSelect').value;
	//create an array of odd and even numbers
	var numberList = [1,6,9,12,15,21];
	//create a field to display the results
	var result = document.getElementById('result3');
	//create values to add to either odd or even totals
	var evenNumber= 0;
	var oddNumber = 0;
	//for loop to parse through the array
	//will run until the array length is met
	for(var i = 0; i < numberList.length; i++){
		//if the number at this point in the array can be divided by 2 without leaving a remainder
		//the number is even
		if(numberList[i] % 2 == 0){
			//add the number to the even total
			evenNumber = +evenNumber + +numberList[i];
		}
		//if the number does leave a remainder, it must be odd
		else{
			//add number to the odd total
			oddNumber = +oddNumber + +numberList[i];
		}
	}
	//check the user input
	//if they entered even
	if(userSelection === 'even'){
		//display the even total
		result.textContent = 'The even numbers add to ' + evenNumber;
	}
	//if they entered odd
	else if(userSelection === 'odd'){
		//display the odd total
		result.textContent = 'The odd numbers add to ' + oddNumber;
	}
	//if the input does not match either of the above
	else{
		//give the user an error, and the accepted inputs
		result.textContent = 'Input not recognized. Please only type "even" or "odd".';
	}
}
//function to keep track of a credit card limit
//will use alerts and prompts to run until the limit is reached
function ChargeIt(){
	//save value of the first textbox
	var creditLimit = document.getElementById('creditLimit').value;
	//while loop to run until the entered value is reached
	while(creditLimit >= 0){
		//prompt the user to enter how much they spent
		//will save the input to a variable
		var spent = prompt('How much did you spend?')
		//subtract the amount entered from the total limit
		creditLimit = +creditLimit - +spent;
		//if the limit is still above 0
		//let the user know how much they spent and how much is remaining
		if(creditLimit > 0){
			//user alert to show this information
			alert('With your purchase for $' + spent + ', you have a remaining limit of $' + creditLimit + '.');
		}
		//if the user's limit is no longer above 0
		//limit was reached
		//show the user how much they went over it by
		else{
			//use math.abs to get the absolute number of the limit
			//will change the negative number to a positive one
			var positiveLimit = Math.abs(+creditLimit);
			//user alert to show this information
			alert('With your purchase for $' + spent + ', you have reached your limit and exceeded it by $' + positiveLimit + '.');
		}
	}
}