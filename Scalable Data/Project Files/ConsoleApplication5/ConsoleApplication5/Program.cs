﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        static void Main(string[] args)
        {
            string fruitTypes = "Cherry, lime, lemon, pear, kumquat";
            string[] fruitArray = fruitTypes.Split(',');
            Console.WriteLine(fruitArray[1]);

            string testAnswer = "This is fun!";
            Console.WriteLine("The index of fun is " + testAnswer.IndexOf("fun"));

            string welcome = "Hello Student";
            Console.WriteLine(welcome.Length);
        }
    }
}
