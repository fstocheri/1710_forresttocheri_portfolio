﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tocheri_Forrest_CountFish
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Forrest Tocheri
             September 8, 2017
             Section 02
             Count Fish
             */

            //Create an array containing all of the fish colors
            string[] fishColors = new string[15] { "red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green", "red","blue","green","red","yellow" };

            //Prompt the user to select a number representing a color and save that as a string variable
            Console.WriteLine("Please make a selection: \n\r(1) for Red\n\r(2) for Blue\n\r(3)for Green\n\r(4) for Yellow");
            string selectionString = Console.ReadLine();
            //Check, first, that the selection made is indeed a number
            int num = -1;
            if(!int.TryParse(selectionString, out num))
            {
                Console.WriteLine("You did not enter a number.");
                return;
            }
            //convert the string to a number and confirm that that number falls between 1 and 4
            int selectionNumb = int.Parse(selectionString);
            if(selectionNumb < 1 || selectionNumb > 4)
            {
                Console.WriteLine("You did not enter a number 1 through 4, please try again.");
                return;
            }
            //create variables representing the count for each color fish
            int redFish = 0;
            int blueFish = 0;
            int greenFish = 0;
            int yellowFish = 0;
            //create a for loop that will go through the array comparing the value with the color choices and adding to their total if they match
            for ( int i = 0; i < fishColors.Length; i++)
            {
                //adds to the total amount of red fish if the string matches
                if(fishColors[i].ToLower() == "red")
                {
                    redFish++;
                }
                //adds to the total amount of blue fish if the string matches
                else if(fishColors[i].ToLower() == "blue")
                {
                    blueFish++;
                }
                //adds to the total amount of green fish if the string matches
                else if(fishColors[i].ToLower() == "green")
                {
                    greenFish++;
                }
                //adds to the total amount of yellow fish if the string matches
                else if(fishColors[i].ToLower() == "yellow")
                {
                    yellowFish++;
                }
            }
            //create a conditional that will output the correct number and color based on the user input
            if(selectionNumb == 1)
            {
                //output text if the user selected 1
                Console.WriteLine("In the fish tank there are {0} fish of the color {1}", redFish, fishColors[0]);
            }
            else if(selectionNumb == 2)
            {
                //output text if the user selected 2
                Console.WriteLine("In the fish tank there are {0} fish of the color {1}", blueFish, fishColors[1]);
            }
            else if(selectionNumb == 3)
            {
                //output text if the user selected 3
                Console.WriteLine("In the fish tank there are {0} fish of the color {1}", greenFish, fishColors[2]);
            }
            else if(selectionNumb == 4)
            {
                //output text if the user selected 4
                Console.WriteLine("In the fish tank there are {0} fish of the color {1}", yellowFish, fishColors[3]);
            }


            /*
             TEST:
             1 chosen = 2 red fish
             2 chosen = 4 blue fish
             3 chosen = 3 green fish
             4 chosen = 1 yellow fish
             5 chosen = you did not enter a number between 1 and 4
             t chosen = you did not enter a number
             3 chosen when 5 new fish were added and only 1 was green = 4 green fish
             */
        }
    }
}
