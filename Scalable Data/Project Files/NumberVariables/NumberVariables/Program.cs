﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberVariables
{
    class Program
    {
        static void Main(string[] args)
        {
            //Numeric Data Types
            //Integers
            //sbyte - signed byte - positive or neg
            sbyte exSbyte = 50;
            Console.WriteLine(exSbyte);

            //Short
            short exShort = 1000;
            Console.WriteLine(exShort);

            //Int
            int exInt = 6000000;
            Console.WriteLine(exInt);

            //Long
            long exLong = 12345678900000;
            Console.WriteLine(exLong);

            //Double
            double exDouble = 67.2343;
            Console.WriteLine(exDouble);

            //Float
            float exFloat = 13.3456f;
            Console.WriteLine(exFloat);

            //Decimal - Use this for Money!
            decimal exDecimal = 16.89m;
            Console.WriteLine(exDecimal);
        }
    }
}
