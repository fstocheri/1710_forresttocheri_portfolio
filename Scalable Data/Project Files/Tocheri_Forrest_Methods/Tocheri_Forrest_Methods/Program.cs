﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tocheri_Forrest_Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Forrest Tocheri
             September 16, 2017
             Methods
             */
            //Problem 1: Painting a wall

            //Tell the user what calculation we are doing
            Console.WriteLine("We are going to calculate the amount of paint needed to paint a wall\r\nI just need some information from you.");

            //Ask the user for the information needed to do the calculation
            Console.WriteLine("What is the width of the wall?");
            string widthString = Console.ReadLine();
            decimal wallWidth;
            //verify that the entered value is a number
            while (!decimal.TryParse(widthString, out wallWidth))
            {
                Console.WriteLine("Please only enter numbers\r\nWhat is the width of the wall?");
                widthString = Console.ReadLine();
            }
            //We will do the same format for each of the inputs we will need from the user
            Console.WriteLine("What is the height of the wall?");
            string heightString = Console.ReadLine();
            decimal wallHeight;
            while (!decimal.TryParse(heightString, out wallHeight))
            {
                Console.WriteLine("Please only enter numbers\r\nWhat is the height of the wall?");
                heightString = Console.ReadLine();
            }
            Console.WriteLine("How many coats of paint will we need?");
            string coatsString = Console.ReadLine();
            decimal coatsNeeded;
            while (!decimal.TryParse(coatsString, out coatsNeeded))
            {
                Console.WriteLine("Please only enter numbers\r\nHow many coats of paint do we need?");
                coatsString = Console.ReadLine();
            }
            Console.WriteLine("What is the surface area that one gallon of paint will cover?(in feet^2)");
            string surfaceAreaString = Console.ReadLine();
            decimal surfaceArea;
            while (!decimal.TryParse(surfaceAreaString, out surfaceArea))
            {
                Console.WriteLine("Please only enter numbers\r\nWhat is the surface area that one gallon of paint will cover?(in feet^2)");
                surfaceAreaString = Console.ReadLine();
            }
            //call our method and use the inputs from the user
            decimal gallonsNeeded = GallonsNeeded(wallWidth, wallHeight, coatsNeeded, surfaceArea);
            //output the calculation clear for the user
            Console.WriteLine("For {0} coats on the wall, you will need {1} gallons of paint", coatsNeeded, gallonsNeeded.ToString("#.##"));

            /*
             TESTS:
             width - 8, height - 10, coats - 2, SA - 300
             RESULTS: "For 2 coats on the wall, you will need .53 gallons of paint"
             width - 30, height - 12.5, coats - 3, SA - 350
             RESULTS: "For 3 coats on the wall, you will need 3.21 gallons of paint"
             width - 50, height - 22, coats - 4, SA - 400
             RESULTS: "For 4 coats on the wall, you will need 11 gallons of paint"
             */

            //Problem 2: Stung!

            //Let the user know what calculation we are doing
            Console.WriteLine("It takes 9 bee strings per pound to kill an animal, let's do that calculation.");
            Console.WriteLine("What is the weight of the animal that was strung?");
            string weightString = Console.ReadLine();
            decimal weightDec;
            //double check that the information entered is a number
            while (!decimal.TryParse(weightString, out weightDec))
            {
                Console.WriteLine("Please only enter numbers\r\nWhat is the weight of the animal that was stung?");
                weightString = Console.ReadLine();
            }
            //Call the function created using the weight that the user entered
            decimal stingsNeeded = BeeStings(weightDec);
            //Output the calculated number
            Console.WriteLine("It takes {0} bee stings to kill this animal", stingsNeeded);

            /*
             TESTS:
             weight - 10
             RETURN: it takes 90 bee stings to kill this animal
             weight - 160
             RETURN: it takes 1440 bee stings to kill this animal
             weight - twenty
             RETURN: re-prompt for number
             weight - 509.5
             RETURN - it takes 4585.5 bee stings to kill this animal
             */


            //Problem 3: Reverse It

            //Hard code the list that we will store in an array
            string[] list1 = new string[7] { "Milk", "Bread", "Apples", "Peanut Butter", "Salami", "Cheese", "Pasta" };
            //Call the newly created function to reverse our list and save it as a new array
            string[] reverseList = ReverseItArray(list1);
            //print the list in the normal order and the reverse order, using a loop
            Console.WriteLine("Your original array was");
            list1.ToList().ForEach(Console.WriteLine);
            Console.WriteLine("and now it is reversed as");
            reverseList.ToList().ForEach(Console.WriteLine);

            /*
             Initial array	– [“apple”,	“pear”,	“peach”,	“coconut”,	“kiwi”]
                    Results	- Your	original	array	was	[“apple”,	“pear”,	“peach”,	“coconut”,	
                    “kiwi”] and	now	it	is	reversed as	[“kiwi”,	“coconut”,	“peach”,	“pear”,	
                    “apple”]
             Initial array	– [“red”,	“orange”,	“yellow”,	“green”, “blue”, ”indigo”,	“violet”]
                    Results	- Your	original	array	was	[“red”,	“orange”,	“yellow”,	“green”,
                    “blue”, ”indigo”,	“violet”] and	now	it	is	reversed as	[“violet”,	“indigo”,	
                    “blue”,	“green”,	“yellow”,	“orange”,	“red”]
             Initial array - ["milk", "bread", "Apples", "Peanut Butter", "Salami", "Cheese", "Pasta"]
                    Results - Your original array was ["milk", "bread", "Apples", "Peanut Butter", "Salami", 
                    "Cheese", "Pasta"] and now it is revbersed as ["Pasta", "Cheese", "Salami", "Peanut Butter",
                    "Apples", "Bread", "Mill"]

             */
        }

        //Create a funtion that will do the calculation given the 4 data sets provided by the user
        public static decimal GallonsNeeded(decimal width, decimal height, decimal coats, decimal surface)
        {
            decimal total = ((width * height) / surface) * coats;
            //return the total so that we can call on this method from our main method
            return total; 
        }

        //Create a method to do the bee string calculation
        public static decimal BeeStings(decimal weight)
        {
            decimal total = weight * 9;
            return total;
        }

        //Create a function that will read through the given array, and create a new array in reverse order
        public static string[] ReverseItArray(string[] temp)
        {
            string[] reversedArr = new string[temp.Length];
            for(int i = 0; i < temp.Length; i++)
            {
                reversedArr[temp.Length - 1 - i] = temp[i];
            }
            return reversedArr;
        }
    }
}
