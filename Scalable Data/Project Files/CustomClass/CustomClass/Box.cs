﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomClass
{
    class Box
    {
        //Member Variable
        //Properties of the class
        //Global only for this class
        //Member Variables are private
        int mLength;
        int mWidth;
        int mHeight;
        string mColor;


        //Create constructor function
        //Add parameters for the member variables
        public Box (int _length, int _width, int _height, string _color)
        {
            //Set the values of the member variables using the parameters
            mLength = _length;
            mWidth = _width;
            mHeight = _height;
            mColor = _color;
        }

        //Create a getter method for the height of the cube
        //Getters should always be public
        public int GetHeight()
        {
            //Returns the value of height
            return mHeight;
        }

        public int GetLength()
        {
            return mLength;
        }

        public int GetWidth()
        {
            return mWidth;
        }

        public string GetColor()
        {
            return mColor;
        }


        //Create a setter for our height
        public void SetHeight(int _height)
        {
            //Add a conditional to test if neg
            if (_height > 0)
            {
                //change the value of the member variables
                mHeight = _height;
            }
            else
            {
                Console.WriteLine("This value can not be negative.\r\n Please try again");
            }
        }

        public void SetLength(int _length)
        {
            mLength = _length;
        }

        public void SetWidth(int _width)
        {
            mWidth = _width;
        }

        public void SetColor(string _color)
        {
            mColor = _color;
        }


        //Create custome method
        public int FindVolume()
        {
            int volume = mWidth * mLength * mHeight;
            return volume;
        }
    }
}
