﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {
            //instantiate a box
            //Call the constructor function
            Box firstBox = new Box(5, 5, 5, "Red");

            Box secondBox = new Box(6, 7, 8, "Blue");


            //call on our custom method
            Console.WriteLine(secondBox.FindVolume());
        }
    }
}
