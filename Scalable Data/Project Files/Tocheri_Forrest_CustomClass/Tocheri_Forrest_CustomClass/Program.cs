﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tocheri_Forrest_CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Forrest Tocheri
             Section 02
             September 20, 2017
             Custom Class
             */



            //instance a new hero with 100 health, 0 minimum health, and 100 current health
            Hero mainHero = new Hero(100, 0, 100);

            //show the hero's member variables to the user
            Console.WriteLine("Our Hero's current status is:\r\nMax Health: {0}\r\nMin Health: {1}\r\nCurrent Health: {2}", mainHero.getMaxHealth(), mainHero.getMinHealth(), mainHero.getCurrHealth());

            //explain how to work the program basics
            Console.WriteLine("Our Hero will gain health or take damage until their health reaches 0.");
            //create a constant loop that will run as long as the hero is not at 0 health
            while (mainHero.getCurrHealth() > mainHero.getMinHealth())
            {
                //ask the user if there was damage taken or health gained
                Console.WriteLine("Did our Hero take damage? Or gain health? (damage/health)");
                //save the response
                string userResponse = Console.ReadLine();
                //check if the response matches damage
                if (userResponse.ToLower() == "damage")
                {
                    //run our class function to alter the hero's health
                    mainHero.takeDamage();
                }
                //check if the reponse matches health
                else if (userResponse.ToLower() == "health")
                {
                    //run our class function to alter the hero's health
                    mainHero.gainHealth();
                }
            }
            //print ending line if the hero falls to 0 health
            //ending the program
            Console.WriteLine("Our Hero has fallen, but will be remembered!");
        }
    }
}
