﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tocheri_Forrest_CustomClass
{
    class Hero
    {
        //set our member variables for our hero
        int mMaxHealth;
        int mMinHealth;
        int mCurrHealth;

        //create a constructor function for our hero
        public Hero(int _maxHealth, int _minHealth, int _currHeath)
        {
            mMaxHealth = _maxHealth;
            mMinHealth = _minHealth;
            mCurrHealth = _currHeath;
        }

        //create a function for each member variable
        //these will be the getters for each member variable
        public int getMaxHealth()
        {
            return mMaxHealth;
        }

        public int getMinHealth()
        {
            return mMinHealth;
        }

        public int getCurrHealth()
        {
            return mCurrHealth;
        }

        //these will be the setters for each member variable
        public void setMaxHealth(int _maxHealth)
        {
            _maxHealth = mMaxHealth;
        }

        //this setter will ensure that we are not setting the minimum health below 0
        public void setMinHealth(int _minHealth)
        {
            while(!(_minHealth >= 0))
            {
                Console.WriteLine("Our Hero's minimum health must be greater than or equal to 0\r\nPlease enter the Hero's minimum health:");
                _minHealth = Convert.ToInt32(Console.ReadLine());
            }
            mMinHealth = _minHealth;
        }


        //this setter will ensure that we are not setting the current health to anything below 0
        public void setCurrHealth(int _currHealth)
        {
            while(!(_currHealth >=0))
            {
                Console.WriteLine("Our Hero's current health must be greater than or equal to 0\r\n Please enter the Hero's current health:");
                _currHealth = Convert.ToInt32(Console.ReadLine());
            }
            mCurrHealth = _currHealth;
        }
        
        //this function will trigger if the user chooses to take damage
        public int takeDamage()
        {
            //create a variable to store the damage taken
            int dmgTaken;
            //ask the user how much damage was taken
            Console.WriteLine("How much damage did our Hero take?");
            //save that value as a tring and create a while loop to make sure the value entered is a number
            string dmgTakenString = Console.ReadLine();
            while(!int.TryParse(dmgTakenString, out dmgTaken))
            {
                Console.WriteLine("Please only enter number values for the damage taken.\r\nHow much damage did our Hero take?");
                dmgTakenString = Console.ReadLine();
            }
            //set the current health to whatever the current health was minus the new damage taken
            int currentHealth = mCurrHealth - dmgTaken;
            //check that the current health is not below the minimum health
            //if the damage hits 0 exactly, it will end the program and our hero will die
            while(!(currentHealth >= mMinHealth))
            {
                Console.WriteLine("That damage killed our Hero!\r\nPlease enter a damage value that will not kill our Hero:");
                dmgTaken = Convert.ToInt32(Console.ReadLine());
            }
            //set the current health member variable and print the response for the user
            mCurrHealth = currentHealth;
            Console.WriteLine("OUCH! Our Hero took {0} damage!\r\nOur Hero's health is now: {1}", dmgTaken, mCurrHealth);
            return mCurrHealth;
        }

        //create a function that will trigger if the user chooses to gain health
        public int gainHealth()
        {
            //create a variable to hold the health gained
            int healthGained;
            //ask for the value
            Console.WriteLine("How much health did our Hero gain?");
            //save the health gained as a string and ensure that it was a number entered
            string gainHealthString = Console.ReadLine();
            while(!int.TryParse(gainHealthString, out healthGained))
            {
                Console.WriteLine("Please only enter number values for the health gained.\r\nHow much health did our Hero gain?");
                gainHealthString = Console.ReadLine();
            }
            //set the current health to the old current health plus the health gained
            int currentHealth = mCurrHealth + healthGained;
            //set a conditional that if they enter a number that will exceed the max health, to just set the current health to the max health
            if(currentHealth > mMaxHealth)
            {
                Console.WriteLine("That health gain would put our Hero past the maximum health, so our Hero is now at full health.");
                mCurrHealth = mMaxHealth;
            }
            //print out the response for the user
            Console.WriteLine("Sweet Relief! Our hero gained {0} health!\r\nOur Hero's health is now: {1}", (healthGained - (currentHealth%mMaxHealth)), mCurrHealth);
            return mCurrHealth;
        }
    }
}
