﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TocheriForrest_FindErrorsCond
{
    class Program
    {
        static void Main(string[] args)
        {
            // Forrest Tocheri
            // September 7, 2017
            // Scalable Data Infrastructures
            // Section 02
            // Find and fix the errors

            String myName = "John Doe";
            String myJob = "Cat Wrangler";

            decimal myRatePerCat = 7.50M;
            decimal totalPay = 0;

            int numberOfCats = 40;
            bool employed = true; //Make sure you use this variable

            Console.WriteLine("Hello!  My name is " + myName + ".");
            Console.WriteLine("I'm a " + myJob + ".");
            Console.WriteLine("My current assignment has me wrangling" + numberOfCats + " cats.");
            Console.WriteLine("So, let's get to work!");

            while (numberOfCats > 0) //Do Not Change This line
            {

                if (employed == true)
                {
                    totalPay += myRatePerCat;
                    Console.WriteLine("I've wrangled another cat and I have made $" + totalPay + " so far.  \r\nOnly " + numberOfCats + " left!");

                }
                else
                {

                    Console.WriteLine("I've been fired!  Someone else will have to wrangle the rest!");
                    return;

                }

                numberOfCats--;

                if (numberOfCats == 5)
                {

                    employed = false;

                }

            }
        }
    }
}
