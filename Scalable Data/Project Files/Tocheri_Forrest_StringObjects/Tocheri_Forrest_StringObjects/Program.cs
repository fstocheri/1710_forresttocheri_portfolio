﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tocheri_Forrest_StringObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Forrest Tocheri
             September 16, 2017
             String Objects
             */


            //Problem 1: Email Address Checker


            //Ask the user for their email address
            Console.WriteLine("Please enter your email address:");
            //save that email to a string
            string email = Console.ReadLine();
            //Call our function to determine if the email is of valid format
            if(IsValidEmail(email) == false)
            {
                //returns if the email is valid
                Console.WriteLine("The email address of {0} is a valid email address", email);
            }
            else
            {
                //returns if the email is invalid
                Console.WriteLine("The email address of {0} is not a valid email address", email);
            }

            /*
             TESTS:
             test@fullsail.edu returns valid
             test@full@sail.edu returns invalid
             test@full sail.edu returns invalid
             fstocheri@fullsail.edu returns valid
             */



            //Problem 2: Seperator Swap Out


            //Gather information from the user and save them to variables
            Console.WriteLine("Pleas enter a list of items seperated by a given string:");
            string list = Console.ReadLine();
            Console.WriteLine("Please enter the seperator that was used:");
            string originalSeperator = Console.ReadLine();
            Console.WriteLine("Please enter a new seperator to use:");
            string newSeperator = Console.ReadLine();

            //create a new variable after sending your old parameters through the function
            string newList = SwapSeperator(list, originalSeperator, newSeperator);
            //Display the new list
            Console.WriteLine("The original String of {0} with the new seperator is {1}", list, newList);


            /*
             TESTS:
             list: 1,2,3,4,5 oSeperators: "," nSeperators: "-"
             RESULTS: 1-2-3-4-5
             list: red:blue:green:pink oSeperators: ":" nSeperators: ","
             RESULTS: red,blue,green,pink
             list: 1=2=3=4=5=6 oSeperators: "=" nSeperators: "+"
             RESULTS: 1+2+3+4+5+6
             */
        }


        //custom function used to check for the given cercumstances
        public static bool IsValidEmail(string emailAddress)
        {
            //set the initial value to false
            bool invalid = false;
            //create a substring of everything after the @ symbol
            string afterAt = emailAddress.Substring(emailAddress.LastIndexOf('@') +1);
            //create a count of the numver of times @ is used
            int count = emailAddress.Split('@').Length - 1;
            //Check if the email entered is empty
            if (string.IsNullOrEmpty(emailAddress))
            {
                invalid = true;
            }
            //check if the email entered contains a space
            else if(emailAddress.Contains(" "))
            {
                invalid = true;
            }
            //check if the counted number of @ symbols is more than 1
            else if(count > 1)
            {
                invalid = true;
            }
            //check if the substring after the @ contains a .
            else if(afterAt.Contains("."))
            {
                invalid = false;
            }
            //otherwise its valid
            else
            {
                invalid = true;
            }
            return invalid;

        }

        //create a new function that will take all the parameters entered
        public static string SwapSeperator(string list, string oSeperator, string nSeperator)
        {
            //create a new variable that will find all of the old seperators and replace them with the new ones
            string newList = list.Replace(oSeperator, nSeperator);
            return newList;
        }
    }
}
