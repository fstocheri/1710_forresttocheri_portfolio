﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tocheri_Forrest_Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Forrest Tocheri
             Section 02
             September 1, 2017
             Arrays Assignment
             */

            //Create your own project and call it Lastname_Firstname_Arrays
            //Copy this code inside of this Main section into your Main Section
            //Work through each of the sections

            //Declare and Define The Starting Number Arrays
            int[] firstArray = new int[4] { 4, 20, 60, 150 };
            double[] secondArray = new double[4] { 5, 40.5, 65.4, 145.98 };

            //Find the total of each array and store it in a variable and output to console
            //Total for firstArray
            //Adding all the individual values
            int totalFirstArray = firstArray[0] + firstArray[1] + firstArray[2] + firstArray[3];
            //Print the new total variable
            Console.WriteLine("The total for the first array is " + totalFirstArray);

            //Total for secondArray
            //Adding all the individual values
            double totalSecondArray = secondArray[0] + secondArray[1] + secondArray[2] + secondArray[3];
            //Print the new total variable
            Console.WriteLine("The toal for the second array is " + totalSecondArray);
            //Calculate the average of each array and store it in a variable and output to console
            //Just a reminder to check the averages with a calculator as well, to make sure they are correct.



            /*
               Create a 3rd number array.  
               The values of this array will come from the 2 given arrays.
                -You will take the first item in each of the 2 number arrays, add them together and then store this sum inside of the new array.
                -For example Add the index#0 of array 1 to index#0 of array2 and store this inside of your new array at the index#0 spot.
                -Repeat this for each index #.
                -Do not add them by hand, the computer must add them.
                -Do not use the numbers themselves, use the array elements.
                -After you have the completed new array, output this to the Console.
             */

            //create a new array that contains the first cell of the two other arrays added together 
            double[] thirdArray = new double[4] { firstArray[0] + secondArray[0], firstArray[1] + secondArray[1], firstArray[2] + secondArray[2], firstArray[3] + secondArray[3]};
            //print the array as a list, for each of the cells, on a seperate line
            thirdArray.ToList().ForEach(Console.WriteLine);




            /*
               Given the array of strings below named MixedUp.  
               You must create a string variable that puts the items in the correct order to make a complete sentence.
                -Use each element in the array, do not re-write the strings themselves.
                -Concatenate them in the correct order to form a sentence.
                -Store this new sentence string inside of a string variable you create.
                -Output this new string variable to the console.
             */

            //Declare and Define The String Array
            string[] MixedUp = new string[] { "but the lighting of a", "Education is not", "fire.", "the filling", "of a bucket," };

            //create the new variable, using the array cells in the correct order
            string correctOrder = MixedUp[1] + " " + MixedUp[3] + " " + MixedUp[4] + " " + MixedUp[0] + " " + MixedUp[2];
            //print the new variable string
            Console.WriteLine(correctOrder);



        }
    }
}
