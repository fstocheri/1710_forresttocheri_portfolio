﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comments_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Forrest Tocheri
             SDI Section
             Assignment Name
             Today's Date
             */
            //This is a single lined comment.
            /*
            This is a multi-lined comment.
            Another line.
             */

            //Console.WriteLine() will put a text string on the console with a new line
            Console.WriteLine("Hello World!");
            Console.WriteLine("This will be on a different line");

            //Console.Write() will not put a new line character
            Console.Write("This is an example of a console write.\r\n");
            Console.Write("Another line of text");
            Console.Write("\r\n");
            Console.Write("Welcome to SDI!");
        }
    }
}
