﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tocheri_Forrest_MadLibs
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Forrest Tocheri
             September 2, 2017
             Section 02
             MadLibs
             */

            //prompt the user for different types of string variables
            Console.WriteLine("We are going to create a madlib!\r\nPlease enter a name and press enter:");

            //catch that input and store it in a new variable
            string nameInput = Console.ReadLine();

            //prompt for a second string variable and catch that in a unique variable
            Console.WriteLine("Please enter a color and press enter:");
            string colorInput = Console.ReadLine();

            //prompt for a third string variable and catch that in its own variable
            Console.WriteLine("Please enter a noun and press enter:");
            string nounInput = Console.ReadLine();

            //prompt for a fourth and final string variable and save that within a variable
            Console.WriteLine("Please enter a verb and press enter:");
            string verbInput = Console.ReadLine();

            //prompt the user for a number and save that within a variable
            Console.WriteLine("Please enter a number and press enter:");
            string firstNumberString = Console.ReadLine();
            //convert the string to a number type
            double firstNumber = double.Parse(firstNumberString);

            //prompt for a second number and save within a variable
            Console.WriteLine("Please enter a second number and press enter:");
            string secondNumberString = Console.ReadLine();
            //convert the string into a number
            double secondNumber = double.Parse(secondNumberString);

            //prompt the user for a third number and save within a varible
            Console.WriteLine("Please enter a third number and press enter:");
            string thirdNumberString = Console.ReadLine();
            //convert the string to a number
            double thirdNumber = double.Parse(thirdNumberString);

            //put the three number inputs into an array
            double[] numberArr = new double[3] { firstNumber, secondNumber, thirdNumber };
            //testing the array
            //Console.WriteLine(numberArr[1]);

            //create a story with the given inputs
            Console.WriteLine("There once was a frog named " + nameInput + " who lived with its " + numberArr[0] + " brothers and sisters. They all lived in their " + colorInput + " " + nounInput + ". One day, " + nameInput + " " + verbInput + "ed" + " " + numberArr[1] + " miles away, leaving his family behind. " + "The journey lasted " + numberArr[2] + " years, but when " + nameInput + " returned, the knowledge gained was worth it. " +nameInput+ " learned a lot visiting many other "+nounInput+"s." +nameInput+" was able to better their home "+nounInput+".");
        }
    }
}