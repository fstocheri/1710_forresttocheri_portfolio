﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tocheri_Forrest_RestaurantCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Forrest Tocheri
             September 2, 2017
             Section 02
             Restaurant Calculator
             */

            //prompt the user for the first bill total.
            //all user inputs will be a string
            Console.WriteLine("We will calculate the total of all your bills, as well as the tip. \r\nPlease enter the total for the first bill and press enter:");

            //catch that user input and store it in a variable
            //again, it will be stored initially with a string
            string firstTotalString = Console.ReadLine();

            //convert string to a number type
            double firstTotalNumber = double.Parse(firstTotalString);

            //Test to make sure the string was converted
            //Console.WriteLine(firstTotalNumber+10);

            //ask user for the second bill, similar to the first
            Console.WriteLine("Plese enter the total for the second bill and press enter:");

            //catch the second string entered and store in a new variable
            string secondTotalString = Console.ReadLine();

            //convert the new input into a decimal
            double secondTotalNumber = double.Parse(secondTotalString);

            //test that the string was converted
            //Console.WriteLine(secondTotalNumber + firstTotalNumber);

            //ask for the third and final bill total
            Console.WriteLine("Please enter the total for the third bill and press enter:");

            //catch the third string and store it in a new string variable
            string thirdTotalString = Console.ReadLine();

            //convert the new inut into a decimal type
            double thirdTotalNunber = double.Parse(thirdTotalString);

            //add the three inputs together to get a sub-total
            //save the sub-total in a new variable to use later
            double subTotal = firstTotalNumber + secondTotalNumber + thirdTotalNunber;

            //print the sub-total for the use to see
            //show sub-total to the second decimal point
            Console.WriteLine("Your sub-total is: "+ subTotal.ToString("#.##"));

            //prompt the user for a tip[ percentage
            //store in a string and convert later
            Console.WriteLine("Please enter a tip percentage based on our service today: ");

            //catch the tip percentage
            string tipString = Console.ReadLine();

            //convert the user input into a number
            //use double in order to convert to percentage
            double tipNumber = double.Parse(tipString);

            //move the decimal for the percentage to help with calculations
            double tipPercentage = tipNumber * .01;

            //test that the tip wsa converted correctly
            //Console.WriteLine(tipPercentage);

            //show the total price of the tip
            double tipTotal = tipPercentage * subTotal;

            //test that the calculation was correct
            //Console.WriteLine(tipTotal);

            //print the tip total to show the user
            //show the total for the tip to the second decimal point
            Console.WriteLine("Your total tip is: " + tipTotal.ToString("#.##"));

            //calculate and print the grand total by adding the sub-total to the tip total
            //show the grand total to the second decimal point
            double grandTotal = subTotal + tipTotal;
            Console.WriteLine("Your grand total is: " + grandTotal.ToString("#.##"));

            //show split option if the users would like to split the check
            //convert the division to the second decimal place
            double splitCheck = grandTotal / 3;
            splitCheck = Math.Round(splitCheck, 2);
            //give user the option
            //show the total to the second decimal point
            Console.WriteLine("If you would like to split the check, your equal split is: " + splitCheck.ToString("#.##"));


        }
    }
}
