﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tocheri_Forrest_FinalProject
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Forrest Tocheri
             September 18, 2017
             Section 02
             Final Project
             */

            //prompt the user for a list of events, seperated by commas and save that to a variable
            Console.WriteLine("Please enter a list of events, seperated by commas:");
            string userList = Console.ReadLine();
            //check that the entered value is not empty
            while(string.IsNullOrEmpty(userList))
            {
                Console.WriteLine("Plese do not leave blank.\r\nPlease enter a list, seperated by commas:");
                userList = Console.ReadLine();
            }
            //create a new arraylist (because of undetermined size) that will run the user list through our function
            ArrayList userArrayList = TextToArray(userList);
            //create a value that will help us be consistent when looping through the arraylists
            //i am using the length of the list, because that should be the same of all other lists that are created
            int count = userArrayList.Count;
            Console.WriteLine("Your event list is as follows:");
            //loop through the users lists and display each item to the user
            for (int i = 0; i < userArrayList.Count; i++)
            {
                Console.WriteLine(userArrayList[i].ToString());
            }

            //create a new arraylist that will be assigned to the user entered prices, and run it through the custom function
            ArrayList userEvents = PromptForCosts(userArrayList);
            //loop through the list and display the prices for each event in a readable format
            Console.WriteLine("The prices for your events, in order, are as follows:");
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine("For (the) {0} it will cost ${1}", userArrayList[i], userEvents[i]);
            }

            //create a variable for the toal of all events, and run the price list created in the previous function through our sum function
            decimal total = SumOfCosts(userEvents);
            //print the total price of all events that the user entered
            Console.WriteLine("The total cost of all of your events will be ${0}.", total);



            /*
             Event List: "Fiddler on the roof, guardians of the galaxy 2, wonder woman"
                Fiddler on the roof cost: 50.00
                Guardians of the galaxy 2 cost: twelve dollars
                    Re-Prompted for new cost: 12.00
                Wonder woman cost: 15.00
                Total: "The total cost of all of your events will be $77.00."
             */

        }

        //create a custom function that will convert the list enetered to an arraylist
        public static ArrayList TextToArray(string list)
        {
            //split the users list wherever there is a comma and save that to a temporary array
            string[] userArray = list.Split(',');
            ArrayList userArrayList = new ArrayList();
            //convert that array we created to an arraylist, accomodating any array size
            foreach(string i in userArray)
            {
                userArrayList.Add(i);
            }
            //return the arraylist after all conversions
            return userArrayList;
        }

        //create a custom function that will prompt the user for the cost of each event entered
        //this will also validate that the entered value is a number
        public static ArrayList PromptForCosts(ArrayList events)
        {
            //create a temporary arraylist that will be used later
            ArrayList eventsArrayList = new ArrayList();
            //create a count that will keep track of the length of the arraylist
            //could just use events.Count without creating a new variable, but this is clearer
            int count = events.Count;
            //create a variable that will keep track of the events price so that we can validate it
            decimal eventPrice;
            //create a for loop that will loop through our events arraylist and add to our new one
            for(int i = 0; i < count; i++)
            {
                //ask the user for a price for the first event
                Console.WriteLine("Please enter the price for {0}", events[i]);
                //save that price as a string
                string eventPriceString = Console.ReadLine();
                //check that the string entered can be converted to a decimal
                //this is checking that the value is a number
                while(!decimal.TryParse(eventPriceString, out eventPrice))
                {
                    //a kick-back message if the user did not enter a number
                    Console.WriteLine("Please enter only numbers, including cents\r\nPlease enter the price for {0}", events[i]);
                    eventPriceString = Console.ReadLine();
                }
                //add that value to the new arraylist
                eventsArrayList.Add(eventPrice);
            }
            //return the newly created arraylist of prices
            return eventsArrayList;
        }

        //create a function that will add all the prices entered in the previous function
        public static decimal SumOfCosts(ArrayList events)
        {
            //create a base variable for the total to be added to later
            decimal total = 0;
            //loop through the price arraylist, and add the value it contains to the total
            for(int i = 0; i < events.Count; i++)
            {
                //we will have to convert the object to a decimal before adding it
                total += Convert.ToDecimal(events[i].ToString());
            }
            //return the decimal total
            return total;
        }
    }
}
