﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextVariables
{
    class Program
    {
        static void Main(string[] args)
        {
            //variables - Characters

            //Declare a variable
            char firstLetter;

            //Define a variable
            firstLetter = 'A';

            //Print the variable to console
            Console.WriteLine(firstLetter);

            //Declare AND Define a variable in one step
            char secondCharacter = 'b';

            //Print the new variable
            Console.WriteLine(secondCharacter);

            //String of characters - basically anything bigger than a single character
            //Must be surrounded by double quotes

            String wholeSentence = "Hello World!";

            //Print the String
            Console.WriteLine(wholeSentence);
            //Combine strings together
            //use plus sign
            string combinedString = "First Part " + "Second Part";
            Console.WriteLine(combinedString);

            //Spaces
            string firstName = "Forrest";
            string lastName = "Tocheri";
            string wholeName = firstName + " " + lastName;

            Console.WriteLine(wholeName);
        }
    }
}
